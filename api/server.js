const express = require('express');
const cors = require('cors');
const mysql = require('mysql');

const customers = require('./app/customers');
const admins = require('./app/admins');
const transaction = require('./app/transaction');

const app = express();
const port = 8000;

app.use(cors());
app.use(express.json());

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '0777253269s',
    database: 'online_shop'
});

connection.connect((err) => {
    if (err) throw err;

    app.use('/admins', customers(connection));
    app.use('/customers', admins(connection));
    app.use('/transaction', transaction(connection));

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});