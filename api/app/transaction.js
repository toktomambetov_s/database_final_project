const express = require('express');

const router = express.Router();

const createRouter = (db) => {
    router.post('/', (req, res) => {
        db.query('UPDATE Customer SET balance = balance - ? ' +
            'WHERE id = (?) ' +
            'AND balance >= ?', [req.body.price, req.body.customerId, req.body.price], function (error, results, fields) {
            if (error) throw error;

            console.log('RESULTS:______', results);

            db.query('UPDATE Admins SET balance = balance + ? ' +
                'WHERE id = 1', [req.body.price], function (error, results, fields) {
                if (error) {
                    db.rollback(function() {
                        throw error;
                    });
                }
                db.commit(function(error) {
                    if (error) {
                        db.rollback(function() {
                            throw error;
                        });
                    }
                    res.send(results);
                });
            });
        });

        console.log(req.body.price);
    });

    return router;
};

module.exports = createRouter;